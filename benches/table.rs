use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};

use kmp::kmp_table;

fn criterion_benchmark(c: &mut Criterion) {
    let max_power: usize = 16;

    let mut group = c.benchmark_group("table generation");
    for power in 0..=max_power {
        let elements = 2_usize.pow(power as u32);
        let vector = (0..=elements).collect::<Vec<usize>>();

        group.throughput(Throughput::Elements(elements as u64));
        group.bench_with_input(BenchmarkId::from_parameter(elements), &vector, |b, input| {
            b.iter(|| kmp_table(&input));
        });
    }
    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
